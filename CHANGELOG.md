## 1.0.2

* Disable do_not_use_environment couse of Dart Defines

## 1.0.1

* Update lint rules list
* Activate more lints

## 1.0.0

* Define lint rules currently used in Eliterro projects.
* Make the lint rules severity more strict.
